import java.io.ByteArrayOutputStream
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.apache.tools.ant.taskdefs.condition.Os
import org.codehaus.groovy.ast.tools.GeneralUtils.args

plugins {
    java
    eclipse
    kotlin("jvm") version "1.3.11"
}

group = "it.unibo.alchemist"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven("https://dl.bintray.com/alchemist-simulator/Alchemist/")
    maven("https://dl.bintray.com/protelis/Protelis/")
}

dependencies {
    val alchemistVersion = "8.0.0-beta+0sr.822ad"
    api("it.unibo.alchemist:alchemist:${alchemistVersion}")
    implementation("org.apache.commons:commons-math3:3.6.1")
    implementation(kotlin("stdlib-jdk8"))
    implementation("it.unibo.alchemist:alchemist-implementationbase:${alchemistVersion}")
    implementation("it.unibo.alchemist:alchemist-maps:${alchemistVersion}")
    implementation("it.unibo.alchemist:alchemist-incarnation-protelis:${alchemistVersion}")
    implementation("org.protelis:protelis-interpreter:11.0.0-beta+0oe.49d6")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}
tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

sourceSets.getByName("main") {
    resources {
        srcDirs("src/main/protelis")
    }
}

task("runTests") {
    doLast {
        println("Done.")
    }
}
fun makeTest(
    file: String,
    name: String = file,
    sampling: Double = 1.0,
    time: Double = Double.POSITIVE_INFINITY,
    debug: Boolean = false
) {
    task<JavaExec>("$name") {
        classpath = sourceSets["main"].runtimeClasspath
        classpath("src/main/protelis")
        main = "it.unibo.alchemist.Alchemist"
        if (debug) {
            jvmArgs("-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=1044")
        }
        File("data").mkdirs()
        args(
            "-y", "src/main/yaml/${file}.yml",
            "-g", "src/main/resources/${file}.aes"
        )
    }
    tasks {
        "runTests" {
            dependsOn("$name")
        }
    }
}

makeTest("d1_distance_compare")
makeTest("d2_collection_compare")
makeTest("d3_distance_vienna")
makeTest("d4_voronoi_vienna")

defaultTasks("runTests")
